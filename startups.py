import discord
import asyncio
import utils as u
import functools
from urllib import parse as up
import datetime as dt
import requests as r
import random as ra

startups=[]
msgs=[]

mangalog={} # This is a dict of channel IDs, which in turn contain a dict of manga IDs:message IDs

def insert(loc):
 def _wrap(func):
  loc.append(func)
  return func
 return _wrap

#Startup scripts
@insert(startups)
async def storylogger(client):
 info=u.dbget('chandumps','id, guildid, channelid, lastmessageid, outdir','outdir is not null')
 for entry in info:
  a=client.get_channel(int(entry['channelid']))
  if not a: u.trace(f"Can't into channel {entry['channelid']}"); continue
  #Have to strcmp because guildid could be None, which can't int()
  if not str(a.guild.id)==entry['guildid']:
   if entry['guildid'] is None:
    a=discord.utils.find(lambda c: c.id==int(entry['channelid']),client.private_channels)
   else:
    a=client.get_guild(int(entry['guildid'])).get_channel(int(entry['channelid']))
  try:
   if entry['lastmessageid'] is None:
    #Lag time
    msgs=await a.history(limit=None,oldest_first=True).flatten()
   else:
    msg=await a.fetch_message(int(entry['lastmessageid']))
    msgs=await a.history(limit=None,oldest_first=True,after=msg.created_at).flatten()
  except Forbidden:
   pass
  if msgs:
   story=[n.content for n in msgs]
   u.dbalter('chandumps',{'lastmessageid':msgs[-1].id},f'id="{entry["id"]}"')
   #It is now that I realise that outdir is actually just an outfile.
   with open(entry['outdir'],'a') as out:
    out.write(' '+' '.join(story))

@insert(startups)
async def mangalogger(client):
 info=u.dbget('chandumps','id,guildid,channelid,lastmessageid','outdir is null')
 for n in info: mangalog[n['channelid']]={}
 known=u.dbget('dupeslist')
 for n in known: mangalog[n['chid']][n['maid']]=n['meid']
 for entry in info:
  chan=client.get_channel(int(entry['channelid']))
  if not chan: u.trace(f"Can't into channel {entry['channelid']}"); continue
  if entry['lastmessageid'] is None:
   msgs=await chan.history(limit=None,oldest_first=True).flatten()
  else:
   msg=await chan.fetch_message(int(entry['lastmessageid']))
   msgs=await chan.history(limit=None,oldest_first=True,after=msg.created_at).flatten()
  if not msgs: u.trace(f'Since nothing to do in {chan.id}, why should I get up?'); continue
  cleanup=[]
  for msg in msgs:
   dat=parseMD(msg.content,chan.id,msg.id)
   if dat in (-1,1): cleanup.append(msg)
   if type(dat)==str: await chan.send(dat); cleanup.append(msg)
  cuids=[n.id for n in cleanup]
  cleanup=await chan.purge(limit=None,check=lambda x:x.id in cuids) # A builtin convenience wrapper. Only in Python. :^)
  if len(cleanup)<len(cuids): u.dbalter('chandumps',{'lastmessageid':msgs[-1].id},f'id="{entry["id"]}"') # Since we left some messages
  if len(cleanup)>0: conf=await chan.send(f"Cleaned up {len(cleanup)} non-manga/dupe messages"); await asyncio.sleep(5); await conf.delete()

#Listener scripts
@insert(msgs)
async def deduper(client,msg):
 if msg.channel.id not in [n['channelid'] for n in u.dbget('chandumps','channelid','outdir is null')] or msg.author.bot:
  return
 dat=parseMD(msg.content,msg.channel.id,msg.id)
 if type(dat)==str:
  await msg.channel.send(dat); await msg.delete(); return # Correct from chapter to title
 if dat==-1: msg2="Not an MD link"
 elif dat==1: msg2="Duplicate link"
 if dat in (-1,1):
  msg2=await msg.channel.send(msg2)
  try: await msg.delete()
  except: pass
  await asyncio.sleep(5); await msg2.delete()
 elif dat==0:
  u.dbalter('chandumps',{'lastmessageid':msg.id},f'channelid="{msg.channel.id}"')

def parseMD(link,chanid,msgid):
 replace=False
 parsed=up.urlparse(link)
 if parsed.netloc not in ('mangadex.org','www.mangadex.org','mangadex.cc','www.mangadex.cc'): return -1 #Not an MD link, don't care what it is
 perth=[n for n in parsed.path.split('/') if n!=''] # Strip empty segments, useful for weird cases like mangadex.org//title/id, or just any trailing slash.
 if len(perth)==0: return -1 # Homepage lol
 node=perth[0]
 if node=='chapter':
  # Need to convert it to title link
  # Open the API URL
  mid=r.get(f'https://mangadex.org/api/chapter/{perth[1]}').json()['manga_id']
  parsed=up.urlparse(f'https://mangadex.org/title/{mid}') # Flow onward, but set flag to send special code back
  perth=[n for n in parsed.path.split('/') if n!='']
  node=perth[0]
  replace=True
 if node in ('title','manga'):
  idd=perth[1] # Wao so easy
  if idd in mangalog[chanid]:
   return 1
  else:
   u.dbset('dupeslist',{'maid':idd,'chid':chanid,'meid':msgid})
   mangalog[chanid][idd]=msgid
   if replace: return up.urlunparse(parsed)
   return 0
 else:
  return -1 # Probably a user's page or something.

#nihihi
#@insert(startups)
# Joke's old now.
async def nicks(client):
 guild=client.get_guild(692055691585650739) # Gahaha!
 users={'Trumpa':(363929652248707072,'{}'), 'Toku':(682345849178554408,'{}'), 'Raugg':(196522309991923712,'{}'), 'rust':(465445340439379978,'{}')}#, 'Zerg':(446872504858836995,'{} 青鳥')} # Doesn't work on server owner :(
 nusers=[(guild.get_member(n[0]),n[1]) for n in users.values()]
 with open('objects/junknames.txt') as jn:
  nems=jn.read().split('\n')
 for user,form in nusers:
  await user.edit(nick=form.format(ra.choice(nems))[:32])
