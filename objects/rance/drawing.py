import json as j
with open('objects/rance/data.json') as d:
 data=j.load(d)
classtable=data['classes']

boxes='─│┌┐└┘├┤┬┴┼'

# Generic table creation function. Not used.
def createtable(data):
 #Get the column widths
 colnum=len(data[0])
 #Gotta init this so I can iterate backwards
 colwidths=[0 for _ in range(colnum)]
 for row in data:
  i=colnum
  while i>0:
   #Can't forget this now.
   i-=1
   colwidths[i]=max(colwidths[i],len(str(row[i])))
 #Create header row
 table='┌{0}┐'.format('┬'.join(['─'*width for width in colwidths]))
 #Assume data[0] is the title row
 i=-1
 rowbuild=''
 while i<(colnum-1):
  i+=1
  rowbuild+='│{0}{1}'.format(data[0][i],' '*(colwidths[i]-len(str(data[0][i]))))
 table+='\n│{0}│'.format(rowbuild[1:])
 table+='\n├{0}┤'.format('┼'.join(['─'*width for width in colwidths]))
 #Now the final loop to do the rest of the table, then all I need is the bottom row.
 for row in data[1:]:
  i=-1
  rowbuild=''
  while i<(colnum-1):
   i+=1
   rowbuild+='│{0}{1}'.format(row[i],' '*(colwidths[i]-len(str(row[i]))))
  table+='\n│{0}│'.format(rowbuild[1:])
 table+='\n└{0}┘'.format('┴'.join(['─'*width for width in colwidths]))
 return table

def drawboard(self,unitsize=15):
 # This might become a __str__ alias later.
 
 # How much width each unit is given in the board.
 #unitsize=15
 # How much width the whole board is (generally 4 units+7)
 boardsize=unitsize*4+7

 # Header row, includes player names and scores.
 row1a=f'┌─{self.p1["name"]}─{self.p1["score"]}'
 row1b=f'{self.p2["score"]}─{self.p2["name"]}─┐'
 row1m='─'*(boardsize-len(row1a)-len(row1b))
 row1=row1a+row1m+row1b
 
 # Turn order row. Primarily a list of unit names. Please fill out unit names.
 row2=', '.join(map(lambda x:x.name,self.bs()))
 if len(row2)>boardsize-4:
  row2=row2[:boardsize-7]+'...'
 lon=boardsize-4-len(row2)
 if lon>0:
  row2+=' '*lon
 row2=f'│ {row2} │'
 
 # Spacer row. Static.
 row3='├'+'─'*unitsize+'┬'+'─'*unitsize+'┬─┬'+'─'*unitsize+'┬'+'─'*unitsize+'┤'
 
 # First row of unit data; will do this in blocks of three. Somehow.
 # Units 0, 3, 6 and 9. And "T u r".
 # Haha fuck the above I'm just doing all the chunks and then stitching them together last.
 # Haha fuck *that* above, I'm simplifying by inserting the chunks where they belong instantly.
 rowwho=[[],[],[],[],[],[],[],[],[]]
 i=0
 for unit in self.grid:
  if unit==None:
   rowwho[(i%3)*3].append(' '*unitsize)
   rowwho[(i%3)*3+1].append(' '*unitsize)
   rowwho[(i%3)*3+2].append(' '*unitsize)
   i+=1
   continue
  # Name and flags
  nem2=f'{unit.curflags}/{unit.flags} ' # Hopefully this is never above 9 (3)
  try:
   if i==self.bs()[0].code:
    nem1=' '+unit.name[:unitsize-5-len(nem2)]+'(*)'
   else:
    nem1=' '+unit.name[:unitsize-2-len(nem2)]
  except IndexError:
   nem1=' '+unit.name[:unitsize-2-len(nem2)]
  nem=nem1+' '*(unitsize-len(nem1)-len(nem2))+nem2
  rowwho[(i%3)*3].append(nem)
  # Class and size
  cl=classtable[unit.cls]['icon2']
  siz=f'{unit.cur}/{unit.max}'
  rowwho[(i%3)*3+1].append(' '+cl+' '*(unitsize-4-len(siz))+siz+' ')
  # Status
  rowwho[(i%3)*3+2].append(' '+unit.multistatus()[:unitsize-2]+' '*(unitsize-1-len(unit.multistatus()[:unitsize-2])))
  # If we're at the current active unit's position
  try:
   if i==self.bs()[0].code:
    rowwho[(i%3)*3][-1]=rowwho[(i%3)*3][-1].replace(' ','>',1)
    rowwho[(i%3)*3+1][-1]=rowwho[(i%3)*3+1][-1].replace(' ','>',1)
    rowwho[(i%3)*3+2][-1]=rowwho[(i%3)*3+2][-1].replace(' ','>',1)
  except IndexError:
   pass
  i+=1
 turner=list(f'Turn {self.turn:02}  ')
 rowsir=[]
 for n in rowwho:
  # Pre-prepared turn counter goes in
  n.insert(2,turner.pop(0))
  rowsir.append('│'+'│'.join(n)+'│')
 rowsir='\n'.join(rowsir)
 # Spacer row. Static.
 row7='└'+'─'*unitsize+'┴'+'─'*unitsize+'┴─┴'+'─'*unitsize+'┴'+'─'*unitsize+'┘'
 # Hope it looks right
 return row1+'\n'+row2+'\n'+row3+'\n'+rowsir+'\n'+row7
