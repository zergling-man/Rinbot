import json as j
with open('objects/rance/data.json') as d:
 data=j.load(d)
classtable=data['classes']
statii=data['statii']
from . import actions

class RosterUnit():
 def __init__(self,cls,atk,dfn,inti,spd,size,flags,actis={},name=None):
  self.cls=cls
  self.atk=atk;self.dfn=dfn;self.int=inti;self.spd=spd
  self.max=size
  self.cur=size
  self.flags=flags
  self.name=name or ''
  # Actions that I can't be assed aliasing will just be passed in as a list in the key 0.
  actis.update({k:k for k in actis.pop(0,[])})
  self.actions=actis or {n:getattr(actions,n) for n in classtable[cls]['actions']} # This needs altering later.
  self.actions['rest']=actions.rest
  self.available=True
  self.affection=[0,0] # This should really be a tuple but it can change over time, and it would be annoying to set it instead of just modify it. The two values mean different things, you see.
 
 #This is way too much writing. I should find a way to alias it.
 @property
 def atk(self): return self._atk
 @atk.setter
 def atk(self,val): self._atk=val
 @property
 def dfn(self): return self._def
 @dfn.setter
 def dfn(self,val): self._def=val
 @property
 def int(self): return self._int
 @int.setter
 def int(self,val): self._int=val
 @property
 def spd(self): return self._spd
 @spd.setter
 def spd(self,val): self._spd=val
 
 def rebuild(self):
  return f"Unit('{self.cls}',{self.atk},{self.dfn},{self.int},{self.spd},{self.max},{self.flags},'{self.name}')"
 
 def static(self):
  return f"{self.name}: {self.cls}, ATK: {self.atk}, DEF: {self.dfn}, INT: {self.int}, SPD: {self.spd}, TROOPS: {self.max}, FLAGS: {self.flags}"
 def __str__(self): return static(self) # This should be overridden but the method should continue to be available.

class BattleUnit(RosterUnit):
 def __init__(self,*args,code=None,board=None,**kwargs):
  super().__init__(*args,**kwargs)
  self.curflags=self.flags
  self.code=code
  self.board=board
  # Fuck the Status class
  self.status={}
 
 @RosterUnit.atk.getter
 def atk(self): return self._atk*classtable[self.cls]['atk']*(1+self.status.get('atk',0))
 @RosterUnit.dfn.getter
 def dfn(self): return self._def*classtable[self.cls]['def']*(1+self.status.get('def',0))
 @RosterUnit.int.getter
 def int(self): return self._int*classtable[self.cls]['int']*(1+self.status.get('int',0))
 @RosterUnit.spd.getter
 def spd(self): return self._spd*classtable[self.cls]['spd']*(1+self.status.get('spd',0))
 
 def die(self):
  self.cur=0
  self.status['dead']=True
  self.curflags=0
  return (100,0)
 
 def aspd(self,val):
  # This still has use because of that +4.
  # Actually maybe I will move it out.
  return val/(self.spd+4)
 
 # This is unused now.
 def bigstatus(self):
  try: k=list(filter(lambda x:x in self.status,statii))[0]
  except IndexError: return '' # There is no status
  v=self.status[k]
  if type(v)==bool: return f'{k}'.upper()
  elif type(v)==float: return f'{k}: {v:.3}'.upper() #fk
  else: return f'{k}: {v}'.upper() #fk
 
 def multistatus(self):
  if 'dead' in self.status: return 'DEAD'
  out=[n[0] if n in self.status else ' ' for n in statii]
  if 'g' in out: out[out.index('g')]=f"g{round(self.status['guard']*100)}%"
  out.append(' '*(13-len(out)))
  return ''.join(out)
 
 def __str__(self):
  return f"{self.name}: Troops: {self.cur}/{self.max}, Flags: {self.curflags}/{self.flags}, Status: {self.status}"
 
 @classmethod
 def tobattle(cls,roster):
  out=cls(roster.cls,roster.atk,roster.dfn,roster.int,roster.spd, roster.max,roster.flags,actis=roster.actions,name=roster.name)
  out.cur=roster.cur
  return out
