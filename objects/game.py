import enum

state=enum.Enum('State', ['LOBBY', 'SETUP', 'STARTED', 'FINISHED', 'PAUSED'])

class Game:
 def __init__(self):
  self.state=state.LOBBY # Default to lobby mode
  self.players=[] # Don't wanna be another player losing in this game

 def join(self, player):
  if self.state==0:
   self.players.append(player)
  else: return False # Can't join a running game

 def leave(self, player):
  if player in self.players: # Note you can leave a running game.
   self.players.remove(player) # Eg. when a player is defeated they can be kicked
   return True # if there's no need to keep them there.
   # Can also leave a finished game, for no real reason.
  else: return False # Can't leave a game you're not in.

 def start(self):
  if self.state==0:
   self.state=1
   return self.players # To notify them.
  else: return False # Game not in lobby, can't start.

 def stop(self, winner=None):
  if self.state==1:
   self.state=2
   return winner # Returns the player, list of players, team, etc. that won. "None" if nobody won. (eg. everyone quit)
  else: return False # Game not running!
