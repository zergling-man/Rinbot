# Event stuff.
import utils as u
import pymysql

# I'll move this out to a localisable file later. TODO
strings={'runningevent':"An event is already running on this server.",
'noevent':"There is no event running on this server.",
'eventstarted':"Event started! Players may join in <#{0}> with `{1}hgjoin`.",
'eventended':"Event finished, cleaning up...",
'ispaused':"Event is currently paused.",
'dupejoin':"You're already in this event.",
'joinevent':"Successfully joined the event.",
'noperm':"I don't have permission to modify role {0} on user {1}.",
'leftevent':"You have left the event."}

def createevent(guild,channel,prole,srole=None,playercount=48):
 if not isinstance(guild,int):
  guild=guild.id
 if not isinstance(channel,int):
  channel=channel.id
 if not isinstance(prole,int):
  prole=prole.id
 if not srole:
  srole=prole
 elif not isinstance(srole,int):
  srole=srole.id
 with u.curs() as cursor:
  try:
   cursor.execute(f'insert into events (guildid,channelid,playerrole,specrole,participants) values ("{guild}","{channel}","{prole}","{srole}",{playercount});')
  except pymysql.IntegrityError as dupe:
   if 'Duplicate entry' not in dupe.args[1]:
    raise # I only want to catch duplicates, and silently succeed them, everything else can fuck off up top
   else:
    return False
  cursor.execute(f'select * from events where guildid="{guild}";')
  event=cursor.fetchone()
 u.dbcon.commit()
 return event

def getevent(guild):
 if not isinstance(guild,int):
  guild=guild.id
 with u.curs() as cursor:
  cursor.execute(f'select * from events where guildid={guild}')
  result=cursor.fetchall()
 if len(result)>1:
  raise Exception('There is more than one running event in this guild!?!')
 if not result:
  return False # No event found
 return result[0]

def join(event, participants, player):
 if not isinstance(event,int):
  event=event['id']
 if not isinstance(player,int):
  player=player.id
 with u.curs() as cursor:
  try:
   cursor.execute(f'insert into eventplayers (playerid,eventid) values ("{player}",{event});')
   cursor.execute(f'select * from eventplayers where eventid={event};')
   position=poslist(cursor.fetchall()).index(player)+1
   if position<=participants:
    cursor.execute(f'update eventplayers set position=true where eventid={event} and playerid={player}')
   u.dbcon.commit()
   return position
  except pymysql.IntegrityError as dupe:
   if 'Duplicate entry' in dupe.args[1]:
    return False # Silently 'succeed' dupe issue
   elif 'foreign key constraint' in dupe.args[1]:
    return None # Silently 'succeed' no event issue
   else:
    raise # I only want to catch some things, and silently succeed them, everything else can fuck off up top

def leave(event, player): # This shouldn't ever be needed, but I'll include it for debugging purposes.
 if not isinstance(event,int):
  event=event['id']
 if not isinstance(player,int):
  player=player.id
 with u.curs() as cursor:
  out=cursor.execute(f'delete from eventplayers where eventid={event} and playerid="{player}";') # Return True if left successfully, False if not in event or no event
 u.dbcon.commit()
 return out

def freeze(*,event=None,guild=None):
 if guild is not None and not isinstance(guild,int):
  guild=guild.id
 event=event or getevent(guild)['id']
 if not isinstance(event,int):
  event=event['id']
 with u.curs() as cursor:
  if not cursor.execute(f'update events set active=not active where id={event};'):
   return None # No row was updated, so return "no event"
  cursor.execute(f'select active from events where id={event};')
  out=bool(cursor.fetchone()['active']) # isPaused
 u.dbcon.commit()
 return out

def getplayers(event,spectators=True):
 if not isinstance(event,int):
  event=event['id']
 with u.curs() as cursor:
  if not cursor.execute(f'select * from eventplayers where eventid={event}{"" if spectators else " and position is true"};'):
   return None # Either no event or no players in event.
  return poslist(cursor.fetchall())

def endevent(event):
 if not isinstance(event,int):
  event=event['id']
 with u.curs() as cursor:
  out=cursor.execute(f'delete from events where id={event};')
 u.dbcon.commit()
 return out # True if event deleted, False if it failed (due to no event existing)

def poslist(players): # A utility function to add position information from IDs
 return [int(i['playerid']) for i in sorted(players,key=lambda x:x['id'])]