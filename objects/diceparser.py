# Imports. Important.
import random
import utils as u

tracing=True
ops=[('+','add'),('-',''),('*',''),('/',''),(':',''),('k',''),('t',''),('r',''),('!',''),('d','roll')]
ops=[('+','add'),('d','roll')]

# Funcdef
def roll(input):
 u.trace("rolling")
 if len(input)!=2:
  return input
 u.trace(input)
 results=[int(random.random()*int(input[1]))+1 for _ in range(int(input[0]))]
 u.trace(results)
 return results

#def explode(input):
# pass

def add(input):
 total=sum([sum(i) for i in input])

# Arrange brackets:
def deparen(input):
 # Do it serialised, because it'll be simpler.
 i=0
 down=[]
 store=[]
 while i<len(input):
  if input[i]=='(':
   down.append(i)
  elif input[i]==')':
   start=down.pop()
   stash=input[start:i+1]
   store.append(stash[1:-1])
   input=input.replace(stash,'i'+str(len(store)),1)
   i=start
  i+=1
 store.append(input)
 u.trace(store)
 # Array built. Next trick, parse each one in turn then return.
 i=0
 while len(store)>1:
  store[0]=recurvesplit(store[0],0)
  temp=store.pop(0)
  for j in store:
   if "i"+i in j:
    j=j.replace("i"+i,temp,1)
    break
  i+=1
 store=recurvesplit(store[0],0)
 u.trace(store)
 return store

# Primary driver
def recurvesplit(input, n):
 u.trace(input)
 u.trace(type(input))
 if n<len(ops):
  delim=ops[n][0]
  store=input.split(delim)
  u.trace(ops[n])
  u.trace(store)
  for i in range(len(store)):
   store[i]=recurvesplit(store[i],n+1)
  if ops[n][1]!='':
   # need to convert string to function
   return globals()[ops[n][1]](store)
  print(store)
  return ops[n][0].join(store)
 return input

# Spaghetti code is go
#toparse=input("Enter dice string: ")
#dicearray=[int(n) for n in toparse.split('d')]
#roll(dicearray[0],dicearray[1])
#recurvesplit(toparse, 0)
#deparen(toparse)