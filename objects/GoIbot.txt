When the system has an ingame name, and a Discord server (with a list of members), and a database of matches. Each step is in an attempt to *link* or *review* the name. Each name-account pair can be *certain*, *likely*, *unknown* or *wrong*.
1) If there is a match in the database for this name that is *certain*, assume that user is correct, *link*. If there are multiple matches that are *certain*, throw it to *review*. There shouldn't be more than one Discord account associated with that exact ingame name.
2) Repeat the above for *likely* and *unknown* matches, in that order. However, instead of throwing to review, the oldest (set name first) *unknown* match should be used, and its status promoted to *likely*.
3) Scan members of the faction for a match, with either username or nickname. If there is at least one match, the account with the earliest join date should *link*, and a pair created with status set to *likely*.
4) Repeat the above for leaders of any faction, then members of any faction, and finally all remaining members.

Extras:
When a user sets their own ingame name, a pair is created (or updated) and the status is set to *unknown*.
When a moderator goes to *review*, they go through each pending pair and set the status to whatever they choose. *Wrong* pairs will not be matched; they'll need to be updated (user sets their own ingame name).