excluded=False
commands={'furries':['furs','ff'], 'hacker':[], 'macroadd':['ma'], 'birb':['b','bird'], 'nerfnow':['nn']}

async def furries(args, channel, message):
 await channel.send(content=u.imagemacro('http://rakka.tk/pics/bot/furries/'))

async def macroadd(args, channel, message):
 if int(message.author.id)!=u.owner:
  await channel.send(content="No! Bad!")
  return
 u.macroset(args[0]," ".join(args[1:]))

async def hacker(args, channel, message):
 await channel.send(content=u.imagemacro('http://thelo.ca/hacker/'))

async def birb(args, channel, message):
 #Too lazy to import
 # a=u.r.get('http://random.birb.pw/tweet/').content
 # await channel.send(content='http://random.birb.pw/img/{0}'.format(''.join(map(chr,a))))
 await channel.send(u.imagemacro('http://random.birb.pw/img/'))

async def nerfnow(args, channel, message):
 # Get comic count
 page=str(u.r.get('https://www.nerfnow.com/archives').content)
 start=page.index('<li>')
 #Magic number, moves forward by len('<li><a href="https://www.nerfnow.com/comic/') places
 start+=43
 end=page[start:].index('"')
 count=int(page[start:start+end])
 #Note that pages 1, 2 and 3 don't exist. Don't permit return of those.
 #Return random if not requested specifically
 if not args:
  await channel.send(f'https://www.nerfnow.com/comic/{int(u.ra.random()*(count-3))+4}')
 else:
  if int(args[0]) not in range(4,count+1):
   await channel.send("That page doesn't exist? Well, probably not...")
  else:
   await channel.send(f'https://www.nerfnow.com/comic/{args[0]}')
