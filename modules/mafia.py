excluded=True
commands={'create':['c'], 'destroy':[], 'editmodes':['em'], 'modes':[], 'join':['j'], 'bind':[], 'listbinds':['listchannels'], 'startgame':['start','go'], 'vote':['v'], 'skill':['s'], 'chat':['message', 'm']}

async def create(args, channel, message):
 global games
 games[args[0]]=MGame(message.author, args[0], args[1:])
 await channel.send(content="Mafia game created. Now accepting participants. Name: {0}".format(args[0]))

async def editmodes(args, channel, message):
 global games
 game=games[args[0]]
 game.mode=args[1:]
 await channel.send(content="Game mode successfully altered")

async def destroy(args, channel, message):
 global games
 a=games.pop(args[0],None)
 if a==None:
  await channel.send(content="No such game to destroy")
 else:
  await channel.send(content="Destroyed game {0}".format(args[0]))

async def join(args, channel, message):
 global games
 try:
  games[args[0]].players.add(message.author)
  await channel.send(content="Joined game {0}".format(args[0]))
 except:
  await channel.send(content="Couldn't join game {0}. Check the name.".format(args[0]))