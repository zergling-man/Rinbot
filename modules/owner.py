import time
import discord
import os

excluded=False
commands={'evaluate':['e'], 'addfunc':['af'], 'testfunc':['tf'], 'status':[], 'changename':[], 'lookup':[], 'todoadd':['todo'], 'todoremove':['todorm'], 'todolist':['todols'], 'addtrustee':['at'], 'removetrustee':['rt', 'remtrustee'], 'source':[]}

async def evaluate(args, channel, message):
	if not p.miscperms(2,message.author):
		await channel.send(content='No! Bad!')
		return
	if args[0]=='await':
		await eval(' '.join(args[1:]), globals(), locals())
	else:
		eval(' '.join(args), globals(), locals())

async def addfunc(args, channel, message):
	if not p.miscperms(2,message.author):
		await channel.send(content='No! Bad!')
		return
	toadd=' '.join(args)
	#Add an extra parameter for testfunc
	#I won't always want it, but very frequently I will
	#So save myself typing later
	##Apply a fix for when defining a function with no arguments
	midpend='channel' if '():\n' in toadd else ', channel'
	pos=toadd.index('):\n')
	toadd=toadd[:pos]+midpend+toadd[pos:]
	exec(toadd, globals())

async def testfunc(args, channel, message):
	if not p.miscperms(2,message.author):
		await channel.send(content='No! Bad!')
		return
	if args[0]=='await':
		await globals()[args[1]](*args[2:], channel)
	else:
		globals()[args[0]](*args[1:], channel)

async def changename(args, channel, message):
	if not p.miscperms(2,message.author):
		await channel.send(content='No! Bad!')
		return
	limit=u.dbconfget('ratelimit')
	if limit is None or int(limit) < time.time():
		try:
			await client.user.edit(username=' '.join(args))
			u.trace('Changing')
		except discord.HTTPException as ex:
			u.trace(ex.response)
			u.dbconfset('ratelimit',ex.response.headers['X-Ratelimit-Reset'])
			u.trace('Ratelimited')
	else:
		await channel.send(content=f'Ticktock {int(limit)-time.time()}')

async def status(args, channel, message):
	if not p.miscperms(2,message.author):
		await channel.send(content='No! Bad!')
		return
	type=0
	try:
		type=u.gametypes[args[0]]
	except:
		u.trace('Not a valid playing type!')
	u.dbconfset('status',':'.join((str(type),' '.join(args[1:]))))
	try:
		await client.change_presence(activity=discord.Game(name=' '.join(args[1:]),type=type))
	except:
		u.trace('Failed to set game via command.')

async def lookup(args, channel, message):
	if not p.miscperms(2,message.author):
		await channel.send(content='No! Bad!')
		return
	u.inter=await client.fetch_user(args[0])

async def todoadd(args, channel, message):
	if not p.miscperms(1,message.author):
		await channel.send(content='No! Bad!')
		return
	a=message.author
	u.dbset('todos',{'text':' '.join(args),'who':f'{a.name}#{a.discriminator}'})

async def todoremove(args, channel, message):
	if not p.miscperms(1,message.author):
		await channel.send(content='No! Bad!')
		return
	u.dbdel('todos',f'id={args[0]}')

async def todolist(args, channel, message):
	if not p.miscperms(1,message.author):
		await channel.send(content='No! Bad!')
		return
	todos=u.dbget('todos')
	posty=discord.Embed(title='Todo', type='rich', colour=discord.Colour.from_rgb(0,0,255))
	for item in todos:
		posty.add_field(name=f'{item["id"]}: {item["added"]}', value=f'{item["who"]}\n{item["text"]}')
	await channel.send(embed=posty)

async def addtrustee(args, channel, message):
	if not p.miscperms(2,message.author):
		await channel.send(content='No! Bad!')
		return
	try:
		u.creds['trustees'].extend([int(a) for a in args])
	except KeyError:
		u.creds['trustees']=[int(a) for a in args]
	u.savecreds()

async def removetrustee(args, channel, message):
	if not p.miscperms(2,message.author):
		await channel.send(content='No! Bad!')
		return
	for idn in args:
		try:
			u.creds['trustees'].remove(int(idn))
		except ValueError:
			u.trace(f'ID {idn} not found in trustees to remove')
		except KeyError:
			u.trace('No trustees specified!')
			break
	u.savecreds()

async def source(args, channel, message):
	if len(args)==0:
		# show file list
		files=list(filter(lambda x:x[0] not in '._',os.listdir()))
		await channel.send(f'```{files}```')
		#await channel.send("You thought there was a file list, but it was me, Rin-io!")
		return
	file=os.path.realpath(args[0])
	# Don't allow people to read the credentials file, or anything that isn't in the bot's files.
	# Don't use pathlib for it because pathlib is useless.
	if not file.startswith(os.path.realpath('.')) or file==os.path.realpath('creds.json'): await channel.send("Nice try."); return
	if os.path.isdir(file):
		# sub file list
		files=list(filter(lambda x:x[0] not in '._',os.listdir(file)))
		await channel.send(f'```{files}```')
		return
	try:
		with open(file) as b:
			contents=b.read()
	except FileNotFoundError: await channel.send('No such file'); return
	sections=contents.split('\n\n')
	parsed=[[]]
	for sec in sections:
		sec2=sec.split('\n')
		i=0
		while i<len(sec2) and sec2[i][0] in '@#': i+=1 # Decorators ...And leading comments.
		if i<len(sec2) and ' ' in sec2[i] and sec2[i][:sec2[i].index(' ')] in ['async','def']: parsed.append((sec2,i))
		else: parsed[0].append(sec)
	if len(args)==1:
		# show the headers of the file
		out=''
		for n in parsed: print(n)
		for i in range(1,len(parsed)):
			print(i)
			out+=f'{i}: {parsed[i][0][parsed[i][1]]}\n'
		await channel.send(f'```python\n-1: everything (will probably break)\n0: miscellaneous (imports, globals, etc.)\n{out}```')
		return
	sec=int(args[1])
	if len(args)==2:
		# show the requested section, which could be "all"
		try:
			if sec==-1:
				await channel.send(f'```python\n{contents}```')
			elif sec==0:
				dbn='\n\n'; await channel.send(f'```python\n{dbn.join(parsed[0])}```')
			elif sec>=len(parsed): await channel.send('Derp')
			else: bn='\n'; await channel.send(f'```python\n{bn.join(parsed[sec][0])}```')
		except discord.errors.HTTPException as ex: await channel.send(f'Couldn\'t send, probably too large. Error text:\n{ex}')
