excluded=False
commands={'prefix':['p'], 'unrestrict':[]}

async def prefix(args, channel, message):
 if not p.latentperms(['manage_guild'],channel,message.author):
  await channel.send('Noooope')
  return
 u.guildconfset(channel.guild,'prefix',' '.join(args))
 u.p[channel.guild.id]=' '.join(args)
 await channel.send(content='Prefix changed to {0}'.format(' '.join(args)))

async def unrestrict(args, channel, message):
 if not p.latentperms(['administrator'],channel,message.author):
  await channel.send('Nooooooooope')
  return
 u.guildconfset(channel.guild,'latentperms',args[0])
 await channel.send(content='Permissions {0}restricted'.format('un' if not args[0] else ''))
