import random as ra
import asyncio
from discord.abc import User as duser
from objects import hge
import discord

excluded=False
commands={'rps101':['rps'], 'rpslist':['rpsl'], 'enqueue':['queue','q'], 'listqueue':['lq'], 'popqueue':['pop','pq','next'], 'rikka':[], 'hgopen':['hgo'], 'hgclose':['hgc'], 'hgjoin':['hgj','hg'], 'hglist':['hgl'], 'hgpause':['hgp','hgresume','hgr'], 'hgleave':[]}

async def rps101(args, channel, message):
	names,_,results=u.parserps101()
	size=len(names)
	throw=u.strip(args[0])
	if throw not in names:
		await channel.send('Not a valid choice! Use "rpslist" command!')
	throw=names.index(throw)
	mythrow=ra.randint(0,size-1)
	gap=(throw-mythrow)%size
	if gap==0:
		await channel.send(content="Draw! How unusual! We both picked {0}".format(names[throw].upper()))
	elif gap in range(1,(size+1)//2):
		await channel.send("I picked {2}, so you lost. {1} {0}".format(results[mythrow][gap-1],names[mythrow].upper(),names[mythrow]))
	elif gap in range((size+1)//2,size):
		await channel.send("I picked {2}, so you won! {1} {0}".format(results[throw][size-1-gap],names[throw].upper(),names[mythrow]))
	else:
		await channel.send("Not sure what just happened there... Game broke. My throw: {0} Your throw: {1}".format(mythrow,throw))

async def rpslist(args,channel,message):
	names,_,_=u.parserps101()
	await channel.send(names)

async def enqueue(args, channel, message):
	try:
		u.queue[channel.guild.id].append('{0}#{1}'.format(message.author.name,message.author.discriminator))
	except KeyError:
		u.queue[channel.guild.id]=['{0}#{1}'.format(message.author.name,message.author.discriminator)]
	await channel.send('Queued!')

async def listqueue(args, channel, message):
	if channel.guild.id in u.queue and u.queue[channel.guild.id]:
		await channel.send(', '.join(u.queue[channel.guild.id]))
	else:
		await channel.send('Nobody in queue!')

async def popqueue(args, channel, message):
	pop=args[0] if len(args) else 0
	try:
		out=u.queue[channel.guild.id].pop(pop)
	except KeyError:
		await channel.send("Nobody in queue to remove!")
		return
	await channel.send(out)

async def hgopen(args, channel, message):
	if not p.latentperms(['manage_guild'],channel,message.author):
		await channel.send('You lack permissions for this.')
		return
	if len(args)<1:
		await channel.send('You must specify a role for the players.')
		return
	prole=u.findobj(args[0], channel.guild.roles).id
	ch=channel.id
	srole=prole
	players=48
	if len(args)==2:
		# Got to guess.
		a=u.findobj(args[1], channel.guild.roles)
		b=u.findobj(args[1], channel.guild.channels)
		if a:
			srole=a.id
		elif b:
			ch=b.id
		else:
			players=int(args[1])
	elif len(args)==3:
		srole=u.findobj(args[1], channel.guild.roles).id
		a=u.findobj(args[2], channel.guild.channels)
		if a:
			ch=a.id
		else:
			players=int(args[2])
	elif len(args)==4:
		srole=u.findobj(args[1], channel.guild.roles).id
		a=u.findobj(args[2], channel.guild.channels).id
		players=int(args[3])
	event=hge.createevent(channel.guild.id,channel.id,prole,srole,players)
	if not event:
		await channel.send(hge.strings['runningevent'])
	await channel.send(hge.strings['eventstarted'].format(ch,u.p[channel.guild.id]))

async def hgclose(args, channel, message):
	if not p.latentperms(['manage_guild'],channel,message.author):
		await channel.send('You lack permissions for this.')
		return
	event=hge.getevent(channel.guild)
	if not event:
		await channel.send(hge.strings['noevent'])
		return
	players=hge.getplayers(event['id'])
	hge.endevent(event['id'])
	await channel.send(hge.strings['eventended'])
	players=[u.findobj(x,channel.guild.members) for x in players]
	for user in players:
		if user is None:
			continue
		prole=u.findobj(event['playerrole'],channel.guild.roles)
		srole=u.findobj(event['specrole'],channel.guild.roles)
		try:
			await user.remove_roles(prole, srole, reason="Game over")
		except (discord.Forbidden, discord.HTTPException):
			await channel.send(hge.strings['noperm'].format((prole,srole),user))

async def hglist(args, channel, message):
	event=hge.getevent(channel.guild.id)
	if not event:
		await channel.send(hge.strings['noevent'])
		return
	if not event['active']:
		await channel.send(hge.strings['ispaused'])
		return
	players=hge.getplayers(event['id'],spectators=False)
	players=[u.findobj(pid,channel.guild.members) for pid in players]
	l=int(len(players)/2+0.5)
	firsthalf={i+1:players[i] for i in range(len(players[:l]))}
	secondhalf={i+l+1:players[i+l] for i in range(len(players[l:]))}
	await channel.send('\n'.join(['{0}. {1}#{2} ({3})'.format(pos,member.name,member.discriminator,member.id) for pos,member in firsthalf.items()]))
	await channel.send('\n'.join(['{0}. {1}#{2} ({3})'.format(pos,member.name,member.discriminator,member.id) for pos,member in secondhalf.items()]))

async def hgjoin(args, channel, message):
	event=hge.getevent(channel.guild.id)
	if not event:
		await channel.send(hge.strings['noevent'])
		return
	if not event['active']:
		await channel.send(hge.strings['ispaused'])
		return
	attempt=hge.join(event['id'],event['participants'],message.author.id)
	if attempt==False:
		await channel.send(hge.strings['dupejoin'])
		return
	if attempt<=event['participants']:
		prole=u.findobj(event['playerrole'],channel.guild.roles)
		try:
			await message.author.add_roles(prole, reason="Gametime")
		except (discord.Forbidden, discord.HTTPException):
			await channel.send(hge.strings['noperm'].format(prole,message.author.username))
	else:
		srole=u.findobj(event['specrole'],channel.guild.roles)
		try:
			await message.author.add_roles(srole,reason="Gametime")
		except (discord.Forbidden, discord.HTTPException):
			await channel.send(hge.strings['noperm'].format(srole,message.author.username))
	await channel.send(hge.strings['joinevent'])

async def hgleave(args, channel, message):
	event=hge.getevent(channel.guild.id)
	l=hge.leave(event['id'],message.author.id)
	if l:
		await channel.send(hge.strings['leftevent'])
		prole=u.findobj(event['playerrole'],channel.guild.roles)
		srole=u.findobj(event['specrole'],channel.guild.roles)
		user=message.author
		try:
			await user.remove_roles(prole, srole, reason="Game over")
		except (discord.Forbidden, discord.HTTPException):
			await channel.send(hge.strings['noperm'].format((prole,srole),user))
	else:
		await channel.send(hge.strings['noevent'])

async def hgpause(args, channel, message):
	event=hge.freeze(guild=channel.guild.id)
	if event is None:
		await channel.send(hge.strings['noevent'])
		return
	pause='p' if not event else 'unp'
	await channel.send('Event {}aused.'.format(pause))

async def rikka(args, channel, message):
	u.trace(args)
	main=args[0] if len(args)>0 else '🤔'
	chars=['👈','👇','👉','👆'] if len(args)<2 else args[1:]
	message=await channel.send('{0}{1}{0}'.format(chars[-1],main))
	for i in range(10*len(chars)):
		await asyncio.sleep(0.5) #Uh, not sure if this is imported
		# Oh yeah apparently it is, cool.
		await message.edit(content='{0}{1}{0}'.format(chars[i%len(chars)],main))
