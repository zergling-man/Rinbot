#System imports
import importlib as il
import pip
import discord
import asyncio
import datetime as dt
import pymysql
import json
import os
import time
import random as ra
import traceback

#Bot imports
import startups
import utils as u
import permissions as p

#This has to be in main or bad things happen.
def importer(module, alias=None):
 if alias==None:
  alias=module
 try:
  # Don't even ask. Magic.
  globals()[alias]=il.import_module(module)
 except ModuleNotFoundError:
  u.trace(f'Module {module} not found.')

client=discord.Client()

modules=[file[:-3] for file in os.listdir('modules') if file.endswith('.py')]
for file in modules:
 importer(f'modules.{file}', f'mod{file}')
 mod=globals()[f'mod{file}']
 if not mod.excluded:
  u.commands[f'mod{file}']=mod.commands
  mod.u=u
  mod.p=p
  mod.client=client

#Get token:
try:
 with open('creds.json') as a:
  u.creds=json.load(a)
except FileNotFoundError as e:
 u.trace(e)
 u.trace("Fill out creds.json.ex and rename it to creds.json. Exiting.")
 exit()

#Version number
u.version=1.02
#Defaults
u.p={'default':'>'}
u.owner=u.creds['owner']

try:
 u.dbcon = pymysql.connect(host=u.creds['db']['host'],user=u.creds['db']['user'],password=u.creds['db']['password'],db='rinbot',charset='utf8mb4',cursorclass=pymysql.cursors.DictCursor)
except Exception as e:
 u.trace(e)
 u.trace("Something went wrong connecting to the database.")

@client.event
async def on_ready():
 u.trace(f'Blep! {client.user.name}\n{u.version}\n')
 status=u.dbconfget('status')
 if status is None:
  status="0:"+u.creds['status']
 gethelp = discord.Game(name=status[2:],type=int(status[0]))
 await client.change_presence(activity=gethelp)
 u.trace(list(s.name for s in client.guilds))
 #Initialise prefixes, woohoo!
 prefs=u.guildmassget('prefix')
 for x in prefs: u.p[int(x['guildid'])]=x['prefix']
 for i in startups.startups:
  try:
   await i(client)
  except Exception as e:
   traceback.print_exc() # Don't give up if one fails.

@client.event
async def on_message(message):
 if message.author.bot:
  return
 try:
  prefix=u.p[message.guild.id]
 except (KeyError, AttributeError):
  prefix=u.p['default']
 if message.content.startswith(f'<@{client.user.id}>') or message.content.startswith(f'<@!{client.user.id}>'):
  await message.channel.send(content='{0}help'.format(prefix))
  return
 if message.content==":bird:" or message.content=="🐦":
  await modmacros.birb(None, message.channel, message)
  return
 if message.content.startswith(prefix):
  u.trace("{0}: {1}".format(message.author, message.content))
  inp=message.content[len(prefix):].split(' ')
  com=inp[0]
  args=inp[1:]
  u.trace('Split input into {0} and {1}'.format(com,args))
  #Anti-aliasing. Huehuehue
  for module, commands in u.commands.items():
   for mand, alias in commands.items():
    if com in alias:
     u.trace('Dealiased {0} to {1}'.format(com, mand))
     #This is why I name things in shitty ways, for moments like these
     com=mand
   if com in commands:
    mod=module
  #Like I give a fuck
  try:
   await getattr(globals()[mod],com)(args, message.channel, message)
  except Exception as ex:
   #Ok I give a bit of a fuck
   u.ex=ex
   raise ex
  return
 for i in startups.msgs:
  await i(client,message)

#Force a logout to deal with unclosed sessions.
#It doesn't work anyway.
#And now it's completely broken. I'll solve that issue some other day.
client.run(u.creds['token'])
