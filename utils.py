import datetime as dt
import requests as r
import random as ra
import string as st
import json as j
import discord

commands={}
gametypes={'playing':0,'watching':1,'listening':2}
queue={}

tracing = True

def trace(*message):
 if tracing:
  print("[%s]: %s" % (str(dt.datetime.now()), *message))

def log(*message):
 stanpu=str(dt.date.today())
 taimingu=str(dt.datetime.now().time()) # Why doesn't this have an alias?
 with open(f'logs/{stanpu}','a') as lergg:
  lergg.write(taimingu)
  lergg.write(' ')
  lergg.write('\n'.join(message))
  lergg.write('\n')

def prettify(message):
 if message is not None:
  return '[{message.timestamp.strftime(timestamps)}] {message.author}: {message.content}'

def curs():
 dbcon.ping()
 return dbcon.cursor()

def dbconfget(name):
 with curs() as cursor:
  cursor.execute(f'select value from config where name="{name}"')
  result = cursor.fetchone()
 try:
  return result['value']
 except:
  return None

def dbconfset(name,value):
 with curs() as cursor:
  cursor.execute('insert into config (name, value) values ("{0}", "{1}") on duplicate key update value = "{1}";'.format(name,value))
 dbcon.commit()

def guildmassget(name):
 with curs() as cursor:
  cursor.execute(f'select guildid,{name} from guildconfig')
  result=cursor.fetchall()
 return result

def guildconfget(guild,name):
 with curs() as cursor:
  cursor.execute(f'select {name} from guildconfig where guildid="{guild.id}"')
  result = cursor.fetchone()
 try:
  return result[name]
 except:
  return None

def guildconfset(guild,name,value):
 with curs() as cursor:
  cursor.execute(f'insert into guildconfig (guildid,{name}) values("{guild.id}","{value}") on duplicate key update {name}="{value}"')
 dbcon.commit()

def dbget(table,columns=None,conditions=None):
 if conditions:
  conditions='where '+conditions
 with curs() as cursor:
  cursor.execute(f'select {columns or "*"} from {table} {conditions or ""}')
  result=cursor.fetchall()
 return result

def dbset(table,values):
 if not values:
  return "What the fuck are you doing?"
 with curs() as cursor:
  sss='", "'
  cursor.execute(f'insert into {table} ({", ".join(values.keys())}) values ("{sss.join([str(n) for n in values.values()])}")')
 dbcon.commit()

def dbalter(table,values,conditions=None):
 if not values:
  return "What the fuck are you doing?"
 if conditions:
  conditions='where '+conditions
 update=', '.join([f'{key}="{val}"' for key,val in values.items()])
 with curs() as cursor:
  cursor.execute(f'update {table} set {update} {conditions}')
 dbcon.commit()

def dbdel(table, conditions):
 if conditions:
  conditions='where '+conditions
 with curs() as cursor:
  cursor.execute(f'delete from {table} {conditions}')
 dbcon.commit()

def macrogetrand(cluster):
 with curs() as cursor:
  cursor.execute(f'select * from (select value from macros where cluster="{cluster}") as a order by rand() limit 1')
  result = cursor.fetchone()
 try:
  return result['value']
 except:
  return None

def macroset(cluster,value):
 with curs() as cursor:
  cursor.execute('insert into macros (cluster, value) values ("{cluster}","{value}")')
 dbcon.commit()

def imagemacro(source):
 pics=getpiclist(source)
 return '{0}{1}'.format(source,ra.choice(pics))

def getpiclist(url):
 a=r.get(url).content
 a=(''.join(list(map(chr,a)))).split('href')
 a=list(map(truncatehtml,a))
 return [b for b in a if '.' in b]

def truncatehtml(x):
 if '>' in x:
  x=x[x.index('>')+1:]
 if '<' in x:
  x=x[:x.index('<')]
 return x

def strip(strong):
 return ''.join(a for a in strong.lower() if a in st.digits+st.ascii_lowercase)

def parserps101():
 # File sourced from http://www.umop.com/rps101/alloutcomes.htm
 hands=parseplaintext('objects/rps101.txt')
 # Return a tuple. First one is the name of the throws
 names=[strip(n[n.index('-')+1:]) for n in hands]
 # next is the int
 ints=[n[:n.index(' ')] for n in hands]
 # Last is the results vs other throws
 results=[n for _,n in hands.items()]
 return (names,ints,results)

def parseplaintext(file):
 with open(file) as b:
  a=b.read()
 #Get the major breaks
 a=a.split('\n\n')
 #Get the minor breaks
 a=[n.split('\n') for n in a]
 #Use the first line as a dictionary header
 a={n[0]:n[1:] for n in a}
 return a

def savecreds():
 with open('creds.json','w') as f:
  j.dump(creds,f, ensure_ascii=False, indent=1, separators=(',',': '))

def findobj(a,b):
 return discord.utils.find(lambda x:x.name.lower()==str(a).lower() or x.mention==a or f'<@!{x.id}>'==a or f'<@{x.id}>'==a or str(x.id)==a or x.id==a,b)
