import discord as d
import utils as u

def latentperms(permlist, channel, user):
 if not u.guildconfget(channel.guild,"latentperms"):
  return True
  #And anyway, this will later check the perm stack. Well, it should do it anyway.
 tocheck=d.permissions.Permissions.none()
 tocheck.update(**{n:True for n in permlist})
 if user.permissions_in(channel)>=tocheck:
  return True
 else:
  return False # TODO: Add more detail to this, probably can use A-B

def stackperms(command, channel, user):
 # Get the user's roles
 # Get the channel's guild
 # Pull all relevant perms for that
 # Get the command's module - provide a sensible way of looking that up
 pass

def miscperms(requirement, user):
 # Requirement is an enum
 # 0: Nothing, 1: Trusted, 2: Owner
 # Check if the user's value is at least as high as the requirement
 if requirement==0:
  return True
 if requirement==1:
  return user.id == u.creds['owner'] or user.id in u.creds['trustees']
 if requirement==2:
  return user.id == u.creds['owner']
